﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRenderer : LineDrawer {
	

	// Use this for initialization
	public void DrawCircle () {
		if (form == "звезда") {  // не придумал зачем в задании Clear, решил проверять звезды мы рисуем или треугольники, и очищать если меняем форму
			Clear();
		}
		form = "круг";
		LineRenderer.positionCount++; // увеличиваем кол-во точек на Линии на 1
		if (LineRenderer.positionCount < 3) {LineRenderer.positionCount = 3;} //проверяем чтобы количество линий хватало хотя-бы на треугольник.
		LinesCount = LineRenderer.positionCount; //присваиваем новое значение переменной LinesCount

		for (int i = 0; i < LinesCount; i++) { //цикл который будет вычислять координаты точек у Linerenderer
			//формула правильного многоугольника это х= хi + R * cos (2pi * i /n), где xi это координатный отступ, 
			//R - это радиус описанной окружности, i - порядковое число элемента многоугольника а n - кол-во граней.
			// аналогично с Y, только синус, 5 и 4 координаты центра, а так-же 4 это радиус окружности изначального треугольника. 

			X = 5 + 4 * Mathf.Cos ((-Mathf.PI/3+2 * Mathf.PI * i) / LinesCount); // математическую формулу я взял в википедии
			Y = 4 + 4 * Mathf.Sin ((-Mathf.PI/3+2 * Mathf.PI * i) / LinesCount); // -Mathf.PI/3 это доворачивание треугольника на 90' для красоты
			Z = 10f;
			DrawLine (i, X, Y, Z); 
		}

	}
	public void DrawStar () {

		if (form == "круг") { // не придумал зачем в задании Clear, решил проверять звезды мы рисуем или треугольники, и очищать если меняем форму
			Clear();
		}
		form = "звезда";

		LineRenderer.positionCount = LineRenderer.positionCount+2; // шаг выставлен в 2, для того чтобы получались только нераспадающиеся многоугольники. 
		if (LineRenderer.positionCount <= 3) {LineRenderer.positionCount = 5;} //кол-во точек выставляем в 5, чтобы начать с красивых звездочек
		LinesCount = LineRenderer.positionCount; 
		int nstar = (LinesCount - 1) / 2; //высчитываем основание формулы угла звезды.

		for (int i = 0; i < LinesCount; i=i+1) 
		{ 
			X = 5 + 4 * Mathf.Cos ((2*Mathf.PI * i) /LinesCount*nstar); 
			Y = 4 + 4 * Mathf.Sin ((2*Mathf.PI * i) /LinesCount*nstar); 
			Z = 10f;

			DrawLine (i, X, Y, Z);

		}
	}


		public void Reset () {

			Clear();
			form = "круг";
			LineRenderer.positionCount = 3;
			LinesCount = LineRenderer.positionCount; 

			for (int i = 0; i < LinesCount; i=i+1) 
			{ 
				X = 5 + 4 * Mathf.Cos ((-Mathf.PI/2+2*Mathf.PI * i) /LinesCount); 
				Y = 4 + 4 * Mathf.Sin ((-Mathf.PI/2+2*Mathf.PI * i) /LinesCount); 
			Z = 10f;
			DrawLine (i, X, Y, Z);
			}

	}
}

