﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour {

	public LineRenderer LineRenderer;
	public int LinesCount;
	public int pos; //порядковый номер сегмента
	public string form; //переменная для текущей формы многоугольника
	public float X, Y, Z; // координаты 

	// Use this for initialization
	public void DrawLine (int pos, float X, float Y,float Z) //получаем аргументы 
	{
		LineRenderer.SetPosition(pos, new Vector3(X, Y, Z));  //рисуем линию
	}


	public void Clear () {
		

	for (int i = 0; i < LineRenderer.positionCount; i++) // как оказалось он не забывает все свои позиции при уменьшении числа сегментов.
	{ 
		LineRenderer.SetPosition (i, Vector3.zero); // очищаем координаты для линий.
	}
		LineRenderer.positionCount = 0; 



	}


	




}
	